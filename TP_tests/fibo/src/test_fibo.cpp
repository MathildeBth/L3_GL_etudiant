//
// Created by quentin on 27/04/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include <iostream>
#include "Fibo.hpp"

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, test_fibo)
{
    CHECK_EQUAL(fibo(10),55);
    CHECK_EQUAL(fibo(1),1);
    CHECK_EQUAL(fibo(2),1);
    CHECK_EQUAL(fibo(3),2);
}