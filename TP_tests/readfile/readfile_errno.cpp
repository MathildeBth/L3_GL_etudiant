#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    if (argc != 2) {
        cerr << "usage: " << argv[0] << " <filename>\n";
        exit(-1);
    }

    cout << "Open file\n";
    ifstream file(argv[1]);
    if (not file.good()) {
        cerr << "ifstream failed\n";
        exit(-2);
    }

    cout << "Read N\n";
    uint64_t N;
    file >> N;
    if (not N) {
        cerr << "file is empty\n";
        exit(-3);
    }

    cout << "Allocate V\n";
    int * V =  (int*)malloc(N*sizeof(int));
    if(malloc(N*sizeof(int))== NULL ){
        cerr << "number is too big \n";
        exit(-4);
    }

    cout << "Read V\n";
    for (unsigned i=0; i<N; i++)
        file >> V[i];
    if(*V < N ){
        cerr << "file is too big \n";
        exit(-5);
    }

    cout << "Print V\n";
    for (unsigned i=0; i<N; i++)
        cout << V[i] << " ";
    cout << endl;

    free(V);

    return 0;
}

